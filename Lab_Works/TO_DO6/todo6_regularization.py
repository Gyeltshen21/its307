# -*- coding: utf-8 -*-
"""Todo6_Regularization.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1HE2DWDXsD3zgZAG7aY-4PBuAuZ3QYTqI

# ITS307 Data Analytics                                                   : Spring Semester 2022
# Practical 6
# Regularization
![image.png](attachment:image.png)

# 1. Import Libraries
"""

import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np
from sklearn import datasets
from sklearn.model_selection import train_test_split

boston = datasets.load_boston()
print(boston.DESCR)

"""# 2. Load Data"""

df = pd.DataFrame(data=boston.data,columns=boston.feature_names)
df.head()

X = boston.data
y = boston.target

"""# 3. Train test split"""

#Splitting of the data into Training and Testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20, random_state = 10)

# Checking shape of all splitted data
print("X_train",X_train.shape,"X_test",X_test.shape,"y_train",y_train.shape,"y_test",y_test.shape)

"""# 4. Simple Linear Model"""

from sklearn.linear_model import LinearRegression
modellr = LinearRegression()
modellr.fit(X_train, y_train)

modellr.score(X_train, y_train)

modellr.score(X_test, y_test)

from sklearn.preprocessing import PolynomialFeatures
poly = PolynomialFeatures(degree = 2)
X_train_poly = poly.fit_transform(X_train)
X_test_poly = poly.fit_transform(X_test)

modelp = LinearRegression()
modelp.fit(X_train_poly, y_train)
modelp.score(X_train_poly, y_train)

modelp.score(X_test_poly, y_test)

"""# 5. Ridge Regularization for Linear Model"""

from sklearn.linear_model import Ridge, Lasso
modelr = Ridge(alpha = 0)
modelr.fit(X_train_poly, y_train)
modelr.score(X_train_poly, y_train)

modelr.score(X_test_poly, y_test)

"""# 6. Lasso Regularization for Linear Model"""

modell = Lasso(alpha = 0)
modell.fit(X_train_poly, y_train)
modell.score(X_train_poly, y_train)

modell.score(X_test_poly, y_test)

"""# 7. Polynomial Regression of degree 2"""



"""# 8. Ridge Regularization for Polynomial model"""





"""# 9. Lasso Regularization for Polynomial model"""





"""# 10. Select K Best"""

# implementing feature selection using SelectKBest class
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
kb = SelectKBest(score_func = f_regression, k = 7)
X_selected = kb.fit_transform(X,y)
X_selected.shape

X_train, X_test, y_train, y_test = train_test_split(X_selected, y, test_size = 0.2, random_state = 10)
X_train.shape

lr = LinearRegression()
lr.fit(X_train, y_train)
lr.score(X_train, y_train)

lr.score(X_test, y_test)

df = pd.DataFrame(data = X, columns = boston.feature_names)
df['y'] = y
df.head()

corr = df.corr()
corr

import seaborn as sns
sns.set(rc = {'figure.figsize':(15, 10)})
sns.heatmap(data = corr, annot = True)
plt.show()

columns = ['RM','LSTAT','PTRATIO','INDUS','TAX','NOX','CRIM']

sdf = df[columns]
sdf.head()



"""# 11. Conclusion"""

# To conclude Regularization improves the accuracy of the model and it helps to improve overfitting and unfitting model
# To conclude Select K best improves the accuracy of the model by reducing the number of training data. Which visulizes how our training datas
# are related to each other and it also minizie overfitting and unfitting model